/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package leitor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import join.*;
import leitor.SqlServer;

public class Leitor2{

    private SqlServer server;
    /**
     *
     * @param host Host em que se deseja conectar
     * @param database Nome do database em que se deseja conectar
     * @param user Nome do usuário
     * @param pass Senha do usuário
     * @param tabela nome da tabela que eu quero fazer a pesquisa
     */
    public Leitor2(String host, String database, String user, String pass){
        server = new SqlServer(host, database, user, pass);
    }
    
    /**
     * 
     */
    public void gerarArquivosTabela(String tabela, String id, int numArquivos){
        
        try{
            Connection c = server.getC();
            // turn on support for dbms_output
            CallableStatement cstmt = c.prepareCall("{call dbms_output.enable(32000) }");
            cstmt.execute();

            // run your PL/SQL block
            Statement stmt = c.createStatement();
            String sql =
                "declare  \n" +
                " a number;  \n" +
                " cursor c1 is select" +id+ "from" + tabela + ";  \n" +
                "begin  \n" +
                "  open c1; \n" +
                "  loop \n" +
                "    fetch c1 into a;  \n" +
                "    exit when c1%notfound;  \n" +
                "    dbms_output.put_line('ID: '|| to_char(a)); \n" +
                "  end loop; \n" +
                "end;";
            stmt.execute(sql);

            // retrieve the messages written with dbms_output
            cstmt = c.prepareCall("{call dbms_output.get_line(?,?)}");
            cstmt.registerOutParameter(1,java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(2,java.sql.Types.NUMERIC);

            // escrevendo os arquivos...
            
            for(int z = 0; z < numArquivos; z++){
                
                try{

                    File file = new File(tabela + z + ".csv");
                    FileWriter fstream = new FileWriter(file);
                    BufferedWriter out = new BufferedWriter(fstream);
                    
                    int status = 0;
                    while (status == 0)
                    {
                        cstmt.execute();
                        String line = cstmt.getString(1);
                        status = cstmt.getInt(2);
                        if (line != null && status == 0){
                                out.write(line);
                        }
                        
                    }
                    out.close();
                }catch(IOException e){
                    System.out.println("nao deu pra escrever a linha");
                    e.printStackTrace();
                }
                
            } 
        }catch(SQLException e){
            System.out.println("SQL falhou\n ");
            e.printStackTrace();
        }
    }

}