
import leitor.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import join.*;

public class Main{
    public static void main(String[] args) {
        // caso haja coisa no arquivo, deve ser apagado
        System.out.println("================MAIN=================");
        try{
            if(new File("buckets").isDirectory()){
                for(String s : new File("buckets").list())
                    Files.deleteIfExists(Paths.get("buckets/" + s));
            }
            if(new File("resultados").isDirectory()){
                for(String s : new File("resultados").list())
                    Files.deleteIfExists(Paths.get("resultados/" + s));
            }
            if(new File("buckets-tabela1").isDirectory()){
                for(String s : new File("buckets-tabela1").list())
                    Files.deleteIfExists(Paths.get("buckets-tabela1/" + s));
            }
            if(new File("buckets-tabela2").isDirectory()){
                for(String s : new File("buckets-tabela2").list())
                    Files.deleteIfExists(Paths.get("buckets-tabela2/" + s));
            }
            
            GraceHashJoin g = new GraceHashJoin();

            g.gerarBuckets("tabela1", 2);
            g.gerarBuckets("tabela2", 2);

            g.gerarResultado("buckets-tabela1", "buckets-tabela2");

        }catch(IOException e){
            System.out.println("!!! deu erro!!!");
            e.printStackTrace();
        }
        System.out.println("================/MAIN=================");
    }

}