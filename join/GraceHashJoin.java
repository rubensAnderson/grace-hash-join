/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package join;

import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Hashtable;

/**
 * 
 * <h3> Implementacao do Algoritmo Grace-Hash Join. </h3>
 * 
 * <p> Para evitar o estouro da memoria principal, as tabelas 
 * nao sao carregadas em memoria inteiramente, mas sim particionadas
 * e eh carregado somente uma pequena particao.
 * </p>
 * 
*/
public class GraceHashJoin{

	/**
	 * <h3> Funcao Gerar Tabela </h3>
	 * 
	 * Le um arquivo no padrao csv (separado por virgula) e gera uma Tabela.
	 * 
	 * @param arquivo nome do arquivo que deseja-se transformar em tabela
	 */
	public Tabela gerarTabela(String arquivo) throws IOException{
		System.out.println("==========gerar tabela================");
		Tabela tab = new Tabela(arquivo);

		try {
			FileReader arq = new FileReader(arquivo);
			BufferedReader lerArq = new BufferedReader(arq);
	   
			String linha = lerArq.readLine();

			while (linha != null) {
				//System.out.println(linha);
				Tupla t = new Tupla(linha.split(","));
				tab.addTupla(t);
			  	linha = lerArq.readLine(); // lê da segunda até a última linha
			}
	   
			System.out.println(tab.toString());
			arq.close();

			System.out.println("============gerou tabela================");
			return tab;
		} catch (IOException e) {
			System.out.println("============NAO gerou tabela================");
			System.err.printf("Erro na abertura do arquivo: %s.\n",
			e.getMessage());
		}

		return null;
	}
	

	/**
	 * <h3> Funcao Hash </h3>
	 * 
	 * <p>
	 * Retorna a hashcode da string.
	 *  </p>
	 * @param chave String usada na chave hash
	 */
	private static int hash(String chave){
		return chave.hashCode();
	}

	/**
	 * <h3> Funcao Gerar Buckets </h3>
	 * 
	 * Le uma tabela no diretorio passado como parametro e gera buckets com 
	 * os valores da coluna cujo indice foi passado como parametro
	 * 
	 * @param diretorio caminho ate a tabela no padrao csv
	 * @param chaveDeBusca indice correspondente a coluna cujo valor sera a chave de busca do join
	 */
	public void gerarBuckets(String diretorio, int chaveDeBusca) throws IOException{
		System.out.println("=============Gerar Buckets==============");
		System.out.println(diretorio);
		System.out.println(chaveDeBusca);
		/*
		/diretorio
			/buckets
				1.csv
				...
				n.csv
			tabela1.csv
			...
			tabelan.csv
		*/
		String nomePasta = "buckets-" + diretorio;
		
		// esse diretorio tem todos os arquivos da tabela. ler por ele
		File diret = new File(diretorio);

		if(!diret.exists()){
			System.out.println("Erro! o diretorio passado nao existe!!!");
			return;
		}

		//criando previamnte a pasta com os buckets
		if(!new File(nomePasta).exists()){
			new File(nomePasta).mkdir();
		}else{
			
		}

		// abrindo os arquivos da tabela. 
		if(diret.isDirectory()){
			for(String f : diret.list()){
				// funcao que gera uma Tabela com o conteudo do arquivo da tabela
				Tabela tab = gerarTabela(diretorio + "/" + f);
				
				System.out.println("tabela gerada: ");
				//System.out.println(tab);
				// GERANDO OS BUCKETS... 				
				// para cada tupla, ele gera o hash e usa esse numero para nomear o arquivo que 
				// servira de bucket, salvo na pasta pastaBuckets
				
				for(Tupla <String> t : tab.getTuplas()){

					//hash
					String s = String.valueOf(hash(t.get(chaveDeBusca)));

					// ESSE ARQUIVO EH UM BUCKET
					String dirBuc = nomePasta + "/bucket" + s + ".csv";
					System.out.println(dirBuc);

					File bucket = new File(dirBuc);
					
					if(!bucket.exists()){
						System.out.println("nao existia. criando... ");
						bucket.createNewFile();
					}
				
					System.out.println("escrevendo no arquivo... ");
					//escritor que append a tupla no bucket
					
					System.out.println(t.toString());

					Files.write(Paths.get(dirBuc), t.toCanonicalString().getBytes(), StandardOpenOption.APPEND);
					
				}

			}
		}
		System.out.println("=============/Gerar Buckets==============");
	}

	
	/**
	 * <h3> Funcao Gerar Resultados </h3>
	 * 
	 * <p> Ele checa se ha buckets correspondentes nos diretorios buck1 e buck2, e copia os conteudos deles
	 *  para a pasta resultados </p>
	 * 
	 * @param buck1 caminho ate o bucket1
	 * @param buck2 caminho ate o bucket2
	 */
	public void gerarResultado(String buck1, String buck2) throws IOException{		
		System.out.println("=============Gerar Resultado==============");
		File buckets1;
		File buckets2;
		buckets1 = new File(buck1);
		buckets2 = new File(buck2);
		
		if(!buckets1.isDirectory()){
			System.out.println("ERRO NAO HA BUCKETS 1");
			return;
		}
		
		if(!buckets2.isDirectory()){
			System.out.println("ERRO NAO HA BUCKETS 2");
			return;
		}

		// pasta onde serao salvos os resultados
		String nomePasta = "resultados";
		
		// criando a pasta dos resultados
		if(!new File(nomePasta).exists()){
			new File(nomePasta).mkdir();
		}

		for(String b1 : buckets1.list()){
			for(String b2 : buckets2.list()){
				
				// se o cara b2 do bucket2 esta em bucket1
				// entao copiar todo mundo de bucket1/b1 e bucket2/b2 no resultado/b1
				if(b1.equals(b2)){
					
					System.out.println("Ha correspondencia em " + b1 + " e " + b2);

					String novoResultado = nomePasta + "/" + b1;
					String pathb1 = buck1 + "/" + b1;
					String pathb2 = buck2 + "/" + b2;

					System.out.println("diretorios:");
					System.out.println(novoResultado);
					System.out.println(pathb1);
					System.out.println(pathb2);

					// copio o bucket inteiro na pasta resultado com o nome do b1
					Files.copy(
						Paths.get(pathb1), //src
						Paths.get(novoResultado)
					);
					
					// eh o jeito gerar uma tabela e copiar tudo no arquivo copiado
					Tabela tab = gerarTabela(pathb2);

					for (Tupla t : tab.getTuplas()) {
						Files.write(
							Paths.get(novoResultado), 
							t.toCanonicalString().getBytes(), 
							StandardOpenOption.APPEND
						);
					}
					break;
				}
			}
		}

		System.out.println("=============/Gerar Resultado==============");	
	}

}