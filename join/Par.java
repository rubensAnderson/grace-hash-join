package join;

public class Par <T>{
	public T left, right;
	public Par(T l, T r){
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return "(" + left.toString() + " | " + right.toString() + ")";
	}
}